package com.ait.wine;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/wines")
@Stateless
@LocalBean
public class WineWS {

	@EJB
	private WinesDAO wineDao;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAll() {
		System.out.println("get all wines");
		List<Wine> wines= wineDao.getAllWines();
		return Response.status(200).entity(wines).build();

	}
	
	@GET @Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findWineById(@PathParam("id") int id) {
		Wine wine = wineDao.getWineById(id);
		return Response.status(200).entity(wine).build();
	}
	
	@GET @Path("/winenames/{name}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findWineByName(@PathParam("name") String name) {
		List<Wine> wines= wineDao.getWineByName(name);
		return Response.status(200).entity(wines).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveWine(Wine wine) {
		wineDao.save(wine);
		return Response.status(200).entity(wine).build();
	}
	
	@PUT @Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response updateWine(Wine wine) {
		wineDao.update(wine);
		return Response.status(200).entity(wine).build();
	}
	
	@DELETE @Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteWine(@PathParam("id") int id) {
		wineDao.delete(id);
		return Response.status(204).build();
	}
	    
}