package com.ait.wine;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class WinesDAO {

    @PersistenceContext
    private EntityManager em;
    
    @SuppressWarnings("unchecked")
	public List<Wine> getAllWines() {
       Query query = em.createQuery("SELECT w FROM Wine w");
       return query.getResultList();
    }

	public Wine getWineById(int id) {
		return em.find(Wine.class, id);
	}
	
	public List<Wine> getWineByName(String name) {
		Query query= em.createQuery("SELECT w FROM Wine AS w "+
	"WHERE w.name LIKE ?1");
		query.setParameter(1, "%"+name.toUpperCase()+"%");
		return query.getResultList();
	}

	public void save(Wine wine) {
		em.persist(wine);
	}

	public void update(Wine wine) {
		em.merge(wine);
	}

	public void delete(int id) {
		em.remove(getWineById(id));
	}
  
}
