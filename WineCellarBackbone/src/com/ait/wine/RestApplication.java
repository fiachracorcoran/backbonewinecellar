/**
 * 
 */
package com.ait.wine;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Fiachra Corcoran
 *
 */

@ApplicationPath("/rest")
public class RestApplication extends Application {

}
