var DetailsView = Backbone.View.extend({
	model: Wine,
	id: "addContainer",
	initialize: function(){
	      console.log("init");
	},
	events:{
		  "click #btnSave": "saveWine",
		  "click #btnDelete": "deleteWine",
		  "click #btnAdd": "add",
	  },
	  saveWine: function(e){
		  alert("model"+this.model.get('name'));
		  var wineDetails= {name:$('#name').val(), 
				  			grapes:$('#grapes').val(),
				  			country:$('#country').val(),
				  			region:$('#region').val(),
				  			year:$('#year').val() };
		//  if (this.model.isNew()) {
		  if (($('#wineId').val()) == ''){
			  alert("isnew");
			  this.model= new Wine(wineDetails);
			  alert("test"+this.model.get('name'));
			  wineList.add(this.model);
			  alert("added");
			  this.model.save({
					  name:$('#name').val(),  
					  grapes:$('#grapes').val(), 
					  country:$('#country').val(), 
					  region:$('#region').val(), 
					  year:$('#year').val(),     
					  description:$('#description').val() 
					  },{
						  success:function(wine){
							  alert("sucess - added");
							  $('#wineId').val(wine.id);
						  }
					  });
			  }
		  else{
		  this.model.save({
			  id:parseInt($('#wineId').val()),
			  name:$('#name').val(),  
			  grapes:$('#grapes').val(), 
			  country:$('#country').val(), 
			  region:$('#region').val(), 
			  year:$('#year').val(),     
			  description:$('#description').val() 
			  },{
				  success:function(wine){
					  alert("sucess - updated");
					  mainView.renderList();
				  }
			  });
		  }
		  return false;
	  },
	  deleteWine: function(e){
		  var wineId= parseInt($('#wineId').val());
		  this.model.set({id:wineId});
		  this.model.destroy(
				  {success:function(data){
					  alert("wine deleted");
					  mainView.renderList();
				  }
		  });
		  return false;
	  },
	  
	  add: function(e){
		  $('#btnDelete').hide();
		  alert('Add');
		  $('#wineId').val(null);
		  $('#name').val("");  
		  $('#grapes').val("");
		  $('#country').val("");
		  $('#region').val(""); 
		  $('#year').val("");     
		  $('#description').val("") ;
		  return false;
	  },
	
  render: function(){
	  console.log("render"+this.model.get('name'));
	 var template= _.template($('#wine-details').html(), this.model.toJSON());
	 return this.$el.html(template);
  }
});




