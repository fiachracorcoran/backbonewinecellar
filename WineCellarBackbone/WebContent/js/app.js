
//An instance of MainView is made whenever the app is run.

var mainView;
var wineList;
//The rendered main view is put on the page.
$(document).ready(function(){
	//console.log("here");
	wineList = new WineList();
	wineList.fetch({
		success: function(data){
			mainView = new MainView({collection: wineList});
			mainView.$el.appendTo(document.body);
		}
	});
});

