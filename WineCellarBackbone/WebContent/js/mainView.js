//MainView grabs all the rendered subviews (WineViews) and appends them.
var MainView = Backbone.View.extend({
  collection: WineList,
  id: "container",
  initialize: function(){
   this.listenTo(this.collection, 'add', this.renderList);
   this.render();
  },

  render: function(){
    this.collection.each(function(wine){
    $('#wineList').append(new WineView({model:wine}).render());
    }, this);
    var wine = new Wine(); 
    $('#mainArea').html(new DetailsView({model:wine}).render());
  },
  
  renderList:function(){
	  $('#wineList li').remove();
	  this.collection.each(function(wine){
		    $('#wineList').append(new WineView({model:wine}).render());
		    }, this);
 
  }
});

