
var rootURL = "http://localhost:8080/WineCellarBackbone/rest/wines";

var Wine = Backbone.Model.extend({
	urlRoot:rootURL,
	defaults:{       
		"id":null, 
		"name":"",     
		"grapes":"",  
		"country":"USA", 
		"region":"California",  
		"year":"",      
		"description":"",  
		"picture":""    },
  initialize: function(){
    console.log("wine init");
    this.on('change', function(){
    	console.log('Values for a model have changed');
    });
  }
});

var WineList = Backbone.Collection.extend({
	model: Wine,
	url:rootURL});
