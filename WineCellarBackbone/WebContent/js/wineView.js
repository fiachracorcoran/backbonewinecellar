//WineView builds a list item for each wine.
var WineView = Backbone.View.extend({
  model: Wine,
  tagName:'li',
  events:{
	  "click li": "alertStatus"
  },
  alertStatus: function(e){
	  //alert('Hello');
	  var wine=this.model;
	  $('#mainArea').html(new DetailsView({model:wine}).render());
  },
    render:function(){
		 var template= _.template($('#wine_list').html(), this.model.toJSON());
		return this.$el.html(template);
		
  }
});
